<?php

require_once('sessionoverride.php');



class db2SessionHandling {

    private $logger;
    private $notificationsToEmail = array(
        'gaetane.le.grange@axisinteractive.co.za',
        'jacques@rodel.co.za'
    );
    private $notificationsToSMS   = array(
    );



    private function isLoggedIn() {
        global $f3;

        if ($f3->get('SESSION.user.0.person_id') === null) {
            return FALSE;
        }
        else {
            return TRUE;
        }
    }



    public function blockUser($userEmail) {

        global $f3;

        $db    = $f3->get('DB');
        $flood = new DB\SQL\Mapper($db, 'Flood');

        /*
         * Block the user by adding rows to the flood table
         */
        $counter = 1;

        while ($counter < 5) {

            $flood->ip      = $f3->get('IP');
            $flood->created = time();
            $flood->email   = $userEmail;
            $flood->save();
            $flood->reset();

            $counter++;
        }


        /*
         * Change the password to random - person will have to get their password reset
         * when they are unblocked by SysAdmin
         */
        $person       = new DB\SQL\Mapper($db, 'People');
        $person->load(array('person_email = ?', $userEmail));
        $person->pass = hash_password(\uniqid('', TRUE));
        $person->save();

        /*
         * Log this
         */
        $change = array(
            ':person_id'     => $f3->get('SESSION.user.0.person_id'),
            ':log_date'      => \time(),
            ':log_message'   => 'This user account was blocked due to possible suspicious behaviour.',
            ':log_variables' => NULL,
            ':log_level_id'  => 6,
        );
        \log_change($change);

        return TRUE;
    }



    private function sendSMS($message) {

        $username         = 'jacques@rodel.co.za';
        $password         = 'Rodelsms7722';
        $recipient_number = $message['recipient'];
        $reference        = date("yyyymmddHis");
        $body             = $message['body'];

        $ch       = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://mytxt.co.za/mtx_http');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ("login|$username|$password\r"));
        $response = curl_exec($ch);

        /*
         * Check login was successful and then send
         */
        if (stripos($response, 'login_ack') !== FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, ("send_message|$username|$password|$recipient_number|$reference|$body\r"));
            $response = curl_exec($ch);
        }
        curl_close($ch);

        /*
         * Process message sending response
         */
        if (stripos($response, 'send_message_ack') !== FALSE) {
            /*
             * Message sent
             */
        }
        else {
            /*
             * Send failed
             */
        }
    }



    private function sendEmail($variables) {

        global $settings;
        global $f3;

        require_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer/class.phpmailer.php");
        include_once($_SERVER['DOCUMENT_ROOT'] . "/libraries/PHPMailer/class.smtp.php");

        /*
         * Send the email
         */
        $mail = new PHPMailer();

        //$mail->IsSMTP();
        $mail->isHTML(TRUE);
        $mail->SMTPAuth = true;
        $mail->Host     = $settings['email']['host'];
        $mail->Port     = $settings['email']['port'];
        $mail->Username = $settings['email']['user'];
        $mail->Password = $settings['email']['pass'];

        $mail->AddReplyTo($settings['email']['reply to'], $settings['email']['from name']);
        $mail->SetFrom($settings['email']['from'], $settings['email']['from name']);

        $sent_to = array();
        foreach ($variables['to'] as $name => $address) {
            if (filter_var($address, FILTER_VALIDATE_EMAIL)) {
                if (is_numeric($name)) {
                    $mail->AddAddress($address);
                }
                else {
                    $mail->AddAddress($address, htmlspecialchars($name, ENT_QUOTES, 'UTF-8'));
                }
                $sent_to[] = $address;
            }
        }

        $mail->Subject = $variables['subject'];
        $mail->Body    = $variables['message'];

        if (!empty($variables['attachments'])) {
            foreach ($variables['attachments'] as $file) {
                if (file_exists($file))
                    $mail->AddAttachment($file);
            }
        }

        if ($mail->Send()) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }



    public function initiateSession() {
        $f3         = \Base::instance();
        $sessionDB  = $f3->get('DB');
        global $db2Session;
        $db2Session = new \DB\SQL\SessionOverride($sessionDB, 'sessions', TRUE, function($session) {
            //Overrides the default onsuspicious ???
        });

        $this->logger = new \Log('logs/session.log');
    }



    private function userHasChangedIPS($userID) {

        global $f3;
        global $db2Session;

        /*
         * Check if IP changed during SESSION
         */
        $sessionStartIP = $db2Session->ip();
        $requestIP      = $f3->get('IP');

        if ($sessionStartIP != $requestIP) {
            $this->logger->write('User ID [' . $userID . '] changed IP from >>>' . $sessionStartIP . '<<< to >>>' . $requestIP . '<<<');
            $this->removeAllUserSessions($userID);
            $db2Session->close();
            $f3->reroute('/login/ipchange');

            return TRUE;
        }

        return FALSE;
    }



    private function userHasChangedBrowsers($userID) {
        global $f3;
        global $db2Session;

        $sessionStartBrowser = $db2Session->agent();
        $requestBrowser      = $f3->get('AGENT');

        if ($sessionStartBrowser != $requestBrowser) {
            $this->logger->write('User ID [' . $userID . '] changed browser from >>>' . $sessionStartBrowser . '<<< to >>>' . $requestBrowser . '<<<');
            $this->removeAllUserSessions($userID);
            $db2Session->close();
            $f3->reroute('/login/browserchange');
            return TRUE;
        }

        return FALSE;
    }



    private function trackSecurityBreaches($userEmail, $breachType) {

        global $f3;

        $db = $f3->get('DB');
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $flood = new DB\SQL\Mapper($db, 'FloodSecurity');

        /*
         * Delete everything before today
         */
        $today          = new \DateTime();
        $today->setTime('0', '0', '0');
        $todayTimestamp = $today->getTimestamp();

        try {
            $sql  = "DELETE FROM FloodSecurity WHERE floodsec_created < :floodsec_today";
            $vars = array(
                ':floodsec_today' => $todayTimestamp,
            );

            $f3->get('DB')->exec($sql, $vars);
        }
        catch (\PDOException $e) {

        }

        /*
         * Add a new record
         */
        $flood->reset();
        $flood->floodsec_type    = 1;
        $flood->floodsec_ip      = $f3->get('IP');
        $flood->floodsec_created = \time();
        $flood->floodsec_email   = $userEmail;
        $flood->save();

        /*
         * Check how many exist today for this email account
         */
        $flood->reset();
        $flood->load(array('floodsec_email=? AND floodsec_type=?', $userEmail, $breachType));

        return $flood->count();
    }



    /**
     * Check if user has cellphone set up and email to sms new password
     * No cellphone - block account
     *
     * @param type $uid
     * @return boolean
     */
    private function userHasMultipleSessions($userID) {

        global $f3;
        global $db2Session;

        $sql         = "SELECT * FROM sessions WHERE data REGEXP '.*\"person_id\";s:[0-9]+:\"" . $userID . "\".*'";
        $allSessions = $f3->get('DB')->exec($sql);

        if (count($allSessions) > 1) {

            $this->logger->write('User ID [' . $userID . '] logged in on two different devices.');

            /*
             * Check how many times this has happened. More than 3 = block account
             */
            $userEmail    = $f3->get('SESSION.user.0.person_email');
            $breachNumber = (int) $this->trackSecurityBreaches($userEmail, 1);

            if ($breachNumber >= 3) {
                /*
                 * Send email to let them know what happened
                 */
                $message            = array();
                $message['to'][]    = $f3->get('SESSION.user.0.person_email');
                $message['subject'] = 'Your DealBridge account has been blocked';
                $message['message'] = '
                    <div style="font-family:sans-serif;">
                        <h3>Security Warning</h3>
                        <p>There have been numerous simultaneous logins to your account from multiple devices today. This could mean that someone else may have your password and is trying to login to DealBridge.</p>
                        <p>As a precaution, your password has been reset and your account blocked. Please contact the system\'s administrator to reset your account and your password.</p>
                        <p>:-(</p>
                    </div>
                    ';
                $this->sendEmail($message);

                /*
                 * Send email to security related personnel
                 */
                $message            = array();
                $message['to']      = $this->notificationsToEmail;
                $message['subject'] = 'DealBridge account for *' . $f3->get('SESSION.user.0.person_first_name') . ' ' . $f3->get('SESSION.user.0.person_last_name') . '* has been blocked';
                $message['message'] = '
                    <div style="font-family:sans-serif;">
                        <h3>Security Warning</h3>
                        <p>There have been numerous simultaneous logins to this account from multiple devices today. This could mean that the login details for this account are compromised.</p>
                        <p>As a precaution, the account password has been reset and the account blocked.</p>
                        <p>:-(</p>
                    </div>
                    ';
                $this->sendEmail($message);

                /*
                 * Block the account and close session
                 */
                $this->blockUser($userEmail);
                $this->removeAllUserSessions($userID);
                $db2Session->close();
                $f3->reroute('/login/multiplesessions');
            }
            else {
                $this->removeAllUserSessions($userID);
                $db2Session->close();
                $f3->reroute('/login/multiplesessions');
            }
            return TRUE;
        }
        return FALSE;
    }



    /**
     * Add SMS alert to user who logs in & email
     */
    private function userIsAfterHours($userID) {

        global $f3;
        global $db2Session;

        $now             = new \DateTime();
        $cutoffMorning   = new \DateTime();
        $cutoffMorning->setTime('07', '00', '00');
        $cutoffAfternoon = new \DateTime();
        $cutoffAfternoon->setTime('16', '30', '00');

        /*
         * Between 07:00 and 17:30 - all good
         */
        if ($now < $cutoffAfternoon && $now > $cutoffMorning) {
            return FALSE;
        }

        /*
         * Set up a check so that we only send once per session
         */
        $check = $f3->get('SESSION.securityCheck.afterHours.' . $userID, NULL);

        if (!empty($check)) {
            return FALSE;
        }

        /*
         * Send the email message
         */
        $message['to']      = $this->notificationsToEmail;
        $message['to'][]    = $f3->get('SESSION.user.0.person_email');
        $message['subject'] = 'AFTER HOURS ACTIVITY ALERT';
        $message['message'] = '
            <div style="font-family: sans-serif;">
                <h3>After hours activity alert on www.dealbridge.co.za</h3>
                <table style="width:100%; font-size:80%;">
                    <tr>
                        <td style="width:200px; background:#ccc; padding:10px">Name of person</td>
                        <td style="padding:10px;">' . $f3->get('SESSION.user.0.person_first_name') . ' ' . $f3->get('SESSION.user.0.person_last_name') . '</td>
                    </tr>
                    <tr>
                        <td style="width:200px; background:#ccc; padding:10px">Email</td>
                        <td style="padding:10px;"><a href="mailto:' . $f3->get('SESSION.user.0.person_email') . '">' . $f3->get('SESSION.user.0.person_email') . '</a></td>
                    </tr>
                    <tr>
                        <td style="width:200px; background:#ccc; padding:10px">IP Address</td>
                        <td style="padding:10px;">' . $db2Session->ip() . '</td>
                    </tr>
                    <tr>
                        <td style="width:200px; background:#ccc; padding:10px">Logged in at</td>
                        <td style="padding:10px;">' . date('d/m/Y H:i:s', $db2Session->get('stamp')) . '</td>
                    </tr>
                    <tr>
                        <td style="width:200px; background:#ccc; padding:10px">Something\'s not right...</td>
                        <td style="padding:10px;"><a href="' . $f3->get('REALM') . '?block=' . $f3->get('SESSION.user.0.person_email') . '">Block this account</a></td>
                    </tr>
                </table>
            </div>
            ';

        $emailSent = $this->sendEmail($message);

        /*
         * Get user cellphone number
         */
        $sms['to'] = $this->notificationsToSMS;

        $sql     = ("
                        SELECT * FROM Phone ph
                        LEFT JOIN PeoplePhone pp ON pp.phone_id = ph.phone_id
                        WHERE
                            pp.person_id = :userID
                            AND ph.phone_type_id = 1
                    ");
        $vars    = array(
            ':userID' => $userID,
        );
        $results = $f3->get('DB')->exec($sql, $vars);

        if (!empty($results)) {
            $number      = \str_replace(' ', '', $results[0]['phone_number']);
            $sms['to'][] = $number;
        }

        /*
         * Send SMS
         */
        $sms['body'] = 'ALERT: After hours activity on your Dealbridge account. Not you? ' . $f3->get('REALM') . '?block=' . $f3->get('SESSION.user.0.person_email');

        foreach ($sms['to'] as $recipient) {
            $sms['recipient'] = $recipient;
            $this->sendSMS($sms);
        }

        /*
         * Final logging
         */
        if ($emailSent) {
            /*
             * Only set the check if the email sends, hopefully the failure
             * is only temporary :-/
             */
            $f3->set('SESSION.securityCheck.afterHours.' . $userID, TRUE);
            $this->logger->write('User ID [' . $userID . '] Active after hours - 16:30 and 7:00 - from IP: ' . $db2Session->ip());
        }
    }



    public function removeAllUserSessions($uid) {

        global $f3;
        global $db2Session;

        $sql         = "SELECT * FROM sessions WHERE data REGEXP '.*\"person_id\";s:[0-9]+:\"" . $uid . "\".*'";
        $allSessions = $f3->get('DB')->exec($sql);

        foreach ($allSessions as $session) {
            $db2Session->destroy($session['session_id']);
        }

        $f3->clear('SESSION');
        unset($_SESSION);
    }



    public function checkSession() {

        $f3 = \Base::instance();

        global $db2Session;

        /*
         * Check the user is logged in - not worried if they are guest
         */
        if (!$this->isLoggedIn()) {
            return TRUE;
        }

        $userID = $f3->get('SESSION.user.0.person_id');

        /*
         * Check if the user has multiple sessions open - end all, force password reset
         */
        $manySessions = $this->userHasMultipleSessions($userID);

        /*
         * Check if IP or browser changed during SESSION - log out all user sessions
         */
        $changedIPs     = $this->userHasChangedIPS($userID);
        $changedBrowser = $this->userHasChangedBrowsers($userID);

        /*
         * Check if after 4:30pm and send email warning to Jacques
         */
        $afterHours = $this->userIsAfterHours($userID);
    }

}


