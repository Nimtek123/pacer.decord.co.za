<?php

class UserController {

    private function _utilHashPassword($password) {
        $crypt = \Bcrypt::instance();
        $salt  = \Configuration::$secret;
        $hash  = $crypt->hash($password, $salt);
        return $hash;
    }



    public function routeLoginForm() {

        /*
         * If user is already logged in, sending them away!
         */
        if (\is_logged_in()) {
            $this->f3->reroute('/');
        }

        /*
         * Empty any session data, just in case
         */
        $this->f3->clear('SESSION.user');

        $this->page_content = '/user/views/form_login.htm';
    }



    public function routeLogUserIn() {

        if (!$this->f3->exists('POST.login')) {
            $this->f3->reroute('/');
        }

        /*
         * Check valid username and password
         */
        $user         = new \User\Models\User($this->db, $this);
        $auth         = new \Auth($user, array('id' => 'user_name', 'pw' => 'user_password'));
        $login_result = $auth->login($this->f3->get('POST.user_name'), $this->_utilHashPassword($this->f3->get('POST.user_password')));

        if ($login_result === FALSE) {
            $this->setMessage('error', 'Login details incorrect. If you have forgotten your password, please click on the Lost Password link below.');
            $this->f3->reroute('/user/login');
        }

        /*
         * User is valid, load into session
         */
        $user->loadByColumn('user_name', $this->f3->get('POST.user_name'));

        foreach ($user as $property => $value) {
            $this->f3->set('SESSION.user.' . $property, $value);
        }
        $this->f3->reroute('/');
    }



    public function routeLogUserOut() {
        /*
         * @todo - do this better, delete sessions from DB, etc
         */
        $this->f3->clear('SESSION');
        $this->setMessage('success', 'You have been logged off.');

        $this->f3->reroute('/user/login');
    }

}