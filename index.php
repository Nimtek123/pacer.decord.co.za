<?php

// Kickstart the framework
$f3=require('lib/base.php');

$settings['host'] = 'localhost';
$settings['user'] = 'theuser';
$settings['pass'] = 'thepass';
$settings['database'] = 'decordpacer';
$settings['server'] = 'DEVELOPMENT';

if ($_SERVER['HTTP_HOST'] == 'pacer.decord.co.za') {
	$settings['host'] = 'sql28.jnb2.host-h.net';
	$settings['user'] = 'decorgjtss_2';
	$settings['pass'] = 'QQdZNJw8Kx8';
	$settings['database'] = 'decordpacer';
	$settings['server'] = 'LIVE';
}

$f3->set('servertitle',$settings['server']);

$f3->set('DEBUG',3);
$f3->set('CACHE',FALSE);
ini_set('display_errors', 'On');
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

$f3->set('DB', new DB\SQL(
    'mysql:host='. $settings['host'] .';dbname='. $settings['database'],
    $settings['user'],
    $settings['pass']
));

/*
 * Session handling
 */
if (file_exists('sessionhandling.php')) {
    require_once(dirname(__FILE__) . '/sessionhandling.php');
}
global $sessionHandler;
$sessionHandler = new \db2SessionHandling();
$sessionHandler->initiateSession();

// Load configuration
$f3->config('config.ini');

// load the various components
include_once('routes.php');
include_once('functions.php');
include_once('route_definitions.php');

clean_up();
$sessionHandler->checkSession();
$f3->run();