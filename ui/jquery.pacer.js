// A $( document ).ready() block.
/*
$(function() {
    $( ".datepicker" ).datepicker("option", "dateFormat", "yyyy-mm-dd");
  });
*/
$( document ).ready(function() {
    // code to go here
    $('#goreport').click(function() {
        var url = '/reports/' + $('#report').val() + $('#timedate').val() + $('#account').val();
        window.location.href = url;
    });
    
    $( "input.datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
    
    $('#add-cap-orders').click(function() {
        $('.new-cap-wrapper').show();
    });
    
    $('#add-goggle-orders').click(function() {
        $('.new-goggle-wrapper').show();
    });
    
    $('#add-accessory-orders').click(function() {
        $('.new-accessory-wrapper').show();
    });
    
    $('#job_work #field-account_id-wrapper #account_id').change(function () {
        var acc_id = $(this).children(":selected").prop("value");
        $.ajax({
            url: '/ajax/account_address/'+ acc_id,
            type: "GET",
            success: function(result, status) {
                var fragment = $(result).filter('#ajax_wrapper'); // we only want the content of the AJAX wrapper
                var address = fragment.html();
                $('#delivery_address').val(address);
            },
            error: function (xhr,status,error) {
                
            },
            dataType: 'html'
        });
        
        $.ajax({
            url: '/ajax/account_contacts/'+ acc_id,
            type: "GET",
            success: function(result, status) {
                var fragment = $(result).filter('#ajax_wrapper'); // we only want the content of the AJAX wrapper
                var encoded = fragment.html();
                var options = $('<textarea/>').html(encoded).val();
                $('#job_work #field-person_id-wrapper #person_id').html(options);
            },
            error: function (xhr,status,error) {
                
            },
            dataType: 'html'
        });
    });
    
    $('.change-status').change(function() {
        var id = $(this).attr('id').split('-');
        var sid = $(this).val();
        var ts = Math.round((new Date()).getTime() / 1000);
        window.location.href = '/?s=' + sid + '&j=' + id[2] + '&t=' + ts + '&o=' + id[3];
    });
    
    $('#date-range-picker .form-submit').click(function() {
        var ds = (Date.parse($('#range-start').val())/1000) - (3600*2);
        var df = (Date.parse($('#range-end').val())/1000) + (3600*21.99);
        var rs = $('#range-state').val();
        var ra = $('#range-account').val();
        if (rs != null) {
            window.location.href = '/?a=' + ra + '&ds=' + ds + '&df=' + df + '&v=' + rs;
        }
        else {
            window.location.href = '/?a=' + ra + '&ds=' + ds + '&df=' + df;
        }
    });

    $('#searchModal').on('shown.bs.modal', function () {
      $('#myInput').focus()
    });
    
});
