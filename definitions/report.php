<?php

function report_jobs() {
	global $f3;
	doorman();
	$sql = "SELECT j.job_id, ac.account_name, j.description AS `Job Desc`, j.created, j.deadline, s.status,
            c.cap_description, c.product_code, co.quantity AS `Cap Quantity`, co.number_colours, g.goggle_description, 
            g.goggle_product_code, go.quantity AS `Goggle Quantity`,
            a.accessory_description, a.accessory_product_code, ao.quantity AS `Accessory Quantity`, j.note
            FROM `Job` j
            LEFT JOIN `Account` ac ON j.account_id = ac.account_id
            LEFT JOIN `Status` s ON j.status_id = s.status_id
            LEFT JOIN `CapOrders` co ON j.job_id = co.job_id
            LEFT JOIN `Caps` c ON co.cap_id = c.cap_id
            LEFT JOIN `GoggleOrders` go ON j.job_id = go.job_id
            LEFT JOIN `Goggles` g ON go.goggle_id = g.goggle_id
            LEFT JOIN `AccessoryOrders` ao ON j.job_id = ao.job_id
            LEFT JOIN `Accessories` a ON ao.accessory_id = a.accessory_id
            WHERE j.status_id NOT IN (4,20)";
	
	$result = $f3->get('DB')->exec($sql);
        $result_jobs = [];
        foreach ($result as $row) {
            $result_jobs[] = array ('job_id' => $row['job_id'], 'account_name' => $row['account_name'], 'Job Desc' => $row['Job Desc'], 'created' => gmdate("Y-m-d", $row['created']), 
                                    'deadline' => gmdate("Y-m-d", $row['deadline']), 'status' => $row['status'], 'cap_description' => $row['cap_description'],
                                    'product_code' => $row['product_code'], 'Cap Quantity' => $row['Cap Quantity'], 'number_colours' => $row['number_colours'], 'goggle_description' => $row['goggle_description'], 
                                    'goggle_product_code' => $row['goggle_product_code'], 'Goggle Quantity' => $row['Goggle Quantity'], 'accessory_description' => $row['accessory_description'], 
                                    'accessory_product_code' => $row['accessory_product_code'], 'Accessory Quantity' => $row['Accessory Quantity'], 'note' => $row['note']);
        }
        $fileName = 'jobreport_'.time();
        $headers ='Job ID,Customer,Job Description,Creation Date,Deadline,Status,Cap Description,Cap Code,Cap Quantity,Print Colour Qty,Goggle Description,Goggle Code,
                    Goggle Quantity,Accessory Description,Accessory Code,Accessory Quantity';

        csvDownload($headers, $result_jobs, $fileName); 
        
}
 
function report_customer () {
	global $f3;
        doorman();
	$sql = "SELECT ap.account_id, a.account_name, a.address, a.vat_number, p.person_first_name, p.person_last_name,
                p.person_email, p.person_phone_1, p.person_phone_2
                FROM AccountPerson ap
                LEFT JOIN Account a ON ap.account_id = a.account_id
                LEFT JOIN People p ON ap.person_id = p.person_id";
	
	$result = $f3->get('DB')->exec($sql);
        $fileName = 'customerlist_'.time();
        
        $headers ='Acc ID,Customer Name,Address,VAT No,First Name,Last Name,Email,Phone,Alt Phone';
	
	csvDownload($headers, $result, $fileName); 
        
}

function csvDownload($headers, $arrayData, $fileName)
     {
    global $f3;
         // tell the browser it's going to be a csv file
             //header('Content-Type: text/csv; charset=utf-8');
             // tell the browser we want to save it instead of displaying it
             //header('Content-Disposition: attachment; filename="'.$fileName.'";');
             
            $f = fopen('php://output', 'w');
            fputcsv($f, explode(',',$headers));
            // loop over the input array
            foreach ($arrayData as $row)
             {
                 fputcsv($f, $row,';');
             }
            // tell the browser it's going to be a csv file
            header('Content-Type: application/csv');
            // tell the browser we want to save it instead of displaying it
            header('Content-Disposition: attachment; filename="'.$fileName.'.csv";');
            // make php send the generated csv lines to the browser
            fpassthru($f);
            //exit();
            //$f3->reroute('/');
     }
 
