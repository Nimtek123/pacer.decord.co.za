<?php

function searchGet() {
	global $f3;
	$f3->set('content','search.htm');
	echo Template::instance()->render('layout.htm');
}

function searchPost() {
	global $f3;
	$POST = $f3->get('POST');
	if (!empty($POST['person_first_name'])) {
		$f3->reroute('/people?fname='. htmlspecialchars($POST['person_first_name']));
	}
	elseif (!empty($POST['person_last_name'])) {
		$f3->reroute('/people?lname='. htmlspecialchars($POST['person_last_name']));
	}
	elseif (!empty($POST['account_name'])) {
		$f3->reroute('/accounts?aname='. htmlspecialchars($POST['account_name']));
	}
	elseif (!empty($POST['active_job_name'])) {
		$f3->reroute('/?djname='. htmlspecialchars($POST['active_job_name']));
	}
	elseif (!empty($POST['delivered_job_name'])) {
		$f3->reroute('/?v=d&djname='. htmlspecialchars($POST['delivered_job_name']));
	}
	elseif (!empty($POST['job_id']) && is_numeric($POST['job_id'])) {
		$f3->reroute('/job/'. htmlspecialchars($POST['job_id']));
	}
}

function pacer_ajax() {
	global $f3;
	$op = $f3->get('PARAMS.op');
	$id = $f3->get('PARAMS.id');
	$content = '';
	
	switch($op) {
		case 'account_address': // get default delivery address for account
			$SQL = "SELECT a.address FROM Account a WHERE a.account_id = :aid";
			$vars = array(':aid' => $id);
			$result = $f3->get('DB')->exec($SQL, $vars);
			//$content = '<pre>'. print_r($result, TRUE) .'</pre>';
			if (count($result) && is_array($result[0])) { $content = $result[0]['address']; }
			break;
		case 'account_contacts': // get all people associated with the selected account: person ID, person name.
			// join account to account person to person; get pid, fname, lname
			$opts = array();
			$SQL = 'SELECT SQL_NO_CACHE p.person_id as "pid", concat_ws(" ", p.person_first_name, p.person_last_name) as "pname"
				FROM People p
				JOIN AccountPerson ap ON ap.person_id = p.person_id
				WHERE ap.account_id = :aid
				ORDER by p.person_first_name asc, p.person_last_name asc';
			$vars = array(':aid' => $id);
			$result = $f3->get('DB')->exec($SQL, $vars );
			//print_r($result);
			if (count($result)) {
				foreach ($result as $row) {
					$opts[] =  '<option value="'. $row['pid'] .'">'. htmlspecialchars(trim($row['pname'])) .'</option>';
				}
				$content = implode('', $opts);
			}
			break;
		case 'caps':
			$sql = "SELECT c.cap_id, CONCAT_WS(\" \", c.cap_description, IF(c.cap_requires_print_colours = 1, \"(requires print colours)\", \"\")) as \"cap_description\"
				FROM Caps c ";
			$vars = array();
			if (is_numeric($id)) { $sql .= "WHERE c.cap_id = :cid "; $vars[':cid'] = $id; }
			$sql .= " ORDER BY c.cap_description";
			$result = $f3->get('DB')->exec($sql, $vars);
			if (count($result)) {
				$opts = array();
				foreach ($result as $row) {
					$opts[] = $row['cap_id'] .'::'. htmlspecialchars(strtolower($row['cap_description']));
				}
				$content = implode('|', $opts);
			}
			break;
		case 'goggles':
			$sql = "SELECT g.* from Goggles g ";
			$vars = array();
			if (is_numeric($id)) { $sql .= "WHERE g.google_id = :gid "; $vars[':gid'] = $id; }
			$sql .= " ORDER BY g.goggle_description";
			$result = $f3->get('DB')->exec($sql, $vars);
			if (count($result)) {
				$opts = array();
				foreach ($result as $row) {
					$opts[] = $row['goggle_id'] .'::'. htmlspecialchars(strtolower($row['goggle_description']));
				}
				$content = implode('|', $opts);
			}
			break;
		case 'del_cap_order_img':
			$sql = "UPDATE CapOrders SET artwork_image = NULL WHERE cap_order_id = :cid";
			$f3->get('DB')->exec($sql, array(':cid' => $id));
			// notify whether or not image is null
			$result = $f3->get('DB')->exec("SELECT (co.artwork_image IS NULL) AS \"artwork_img\" FROM CapOrders co WHERE co.cap_order_id = :cid", array(':cid' => $id));
			if (count($result) > 0) {
				$content = $result[0]['artwork_img'];
			}
			break;
		default:
			$content = 'Invalid option';
			break;
	}
	
	$f3->set('content', $content);
	echo Template::instance()->render('ajax.htm');
}