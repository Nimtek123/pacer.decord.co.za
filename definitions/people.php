<?php

function people() {
	global $f3;
	doorman();
	if (isset($_GET['fname'])) {
	 $fname = htmlspecialchars($_GET['fname']);
	 $sql = "SELECT p.* FROM People p WHERE p.person_first_name LIKE '%$fname%' ORDER BY p.person_last_name, p.person_first_name";
	}
	elseif (isset($_GET['lname'])) {
	 $fname = htmlspecialchars($_GET['lname']);
	 $sql = "SELECT p.* FROM People p WHERE p.person_last_name LIKE '%$fname%' ORDER BY p.person_last_name, p.person_first_name";
	}
	else $sql = "SELECT p.* FROM People p order by p.person_last_name, p.person_first_name";
	
	$result = $f3->get('DB')->exec($sql);
	$people = array();
	foreach ($result as $row) {
		$people[] = $row;
	}
	
	$f3->set('people', $people);
	$f3->set('content','people.htm');
	echo Template::instance()->render('layout.htm');
}
 
function peopleAddGet () {
	global $f3;
	$account_id = intval($f3->get('PARAMS.aid'));
	$f3->set('POST.account_id', $account_id);
	
	$sql = "SELECT a.* FROM Account a order by a.account_name";
	$result = $f3->get('DB')->exec($sql);
	$account_options = array('' => '- Select -');
	foreach ($result as $row) {
	 $account_options[$row['account_id']] = $row['account_name'];
	}
	$f3->set('account_options',$account_options);
	$f3->set('title','Create Person');
	$f3->set('content','add_person.htm');
	echo Template::instance()->render('layout.htm');
}
 
function peopleAddPost() {
	global $f3;
	$POST = $f3->get('POST');
	$sql = "INSERT INTO People (person_first_name, person_last_name, person_email, person_phone_1, person_phone_2, staff) VALUES
	 (:person_first_name, :person_last_name, :person_email, :person_phone_1, :person_phone_2, :staff)";
	$vars = array(':person_first_name' => htmlspecialchars($POST['person_first_name']),
								':person_last_name' => htmlspecialchars($POST['person_last_name']),
								':person_email' => htmlspecialchars($POST['person_email']),
								':person_phone_1' => htmlspecialchars($POST['person_phone_1']),
								':person_phone_2' => htmlspecialchars($POST['person_phone_2']),
								':staff' => ((isset($POST['staff']) && $POST['staff'] == 1) ? 1 : 0));
	$f3->get('DB')->exec($sql, $vars);

	$sql = "SELECT MAX(person_id) FROM People";
	$res = $f3->get('DB')->exec($sql);
	$POST['person_id'] = $res[0]['MAX(person_id)'];

	$sql = "INSERT INTO AccountPerson (account_id, person_id) VALUES (:account_id, :person_id)";
	$vars = array(':account_id' => htmlspecialchars($POST['account_id']),
								':person_id' => htmlspecialchars($POST['person_id']));
	$f3->get('DB')->exec($sql, $vars);

	$f3->reroute('/people');
}

function peopleStaff() {
	global $f3;
	$sql = "SELECT p.* FROM People p WHERE p.staff = 1 order by p.person_last_name, p.person_first_name";
	$result = $f3->get('DB')->exec($sql);
	$people = array();
	foreach ($result as $row) {
		$people[] = $row;
	}
	
	$f3->set('people', $people);
	$f3->set('content','staff.htm');
	echo Template::instance()->render('layout.htm');
}

function userRef() {
	global $f3;
	$f3->set('content','userref.htm');
	echo View::instance()->render('layout.htm');
}

function personEditGet() {
	global $f3;
	$pid = intval($f3->get('PARAMS.pid'));
	// get the person's details, including account
	$sql = "SELECT p.*, ap.account_id
					FROM People p LEFT JOIN AccountPerson ap ON ap.person_id = p.person_id
					WHERE p.person_id = :pid";
	$person = $f3->get('DB')->exec($sql, array(':pid' => $pid));
	$p = $person[0];
	$f3->set('POST', $p);
	
	$f3->set('title', 'Edit '. $p['person_first_name'] .' '. $p['person_last_name']);
	$sql = "SELECT a.* FROM Account a order by a.account_name";
	$result = $f3->get('DB')->exec($sql);
	$account_options = array('' => '- Select -');
	foreach ($result as $row) {
	 $account_options[$row['account_id']] = $row['account_name'];
	}
	$f3->set('account_options',$account_options);
	$f3->set('content', 'add_person.htm');
	
	echo Template::instance()->render('layout.htm');
}

function personEditPost() {
	global $f3;
	$POST = $f3->get('POST');
	$person_id = $f3->get('PARAMS.pid');
	$sql = "UPDATE People SET person_first_name = :person_first_name, person_last_name = :person_last_name, person_email = :person_email,
				 person_phone_1 = :person_phone_1, person_phone_2 = :person_phone_2, staff = :staff WHERE person_id = :person_id";
	$vars = array(':person_first_name' => htmlspecialchars($POST['person_first_name']),
							 ':person_last_name' => htmlspecialchars($POST['person_last_name']),
							 ':person_email' => htmlspecialchars($POST['person_email']),
							 ':person_phone_1' => htmlspecialchars($POST['person_phone_1']),
							 ':person_phone_2' => htmlspecialchars($POST['person_phone_2']),
							 ':staff' => ((isset($POST['staff']) && $POST['staff'] == 1) ? 1 : 0),
							 ':person_id' => $person_id);
	$f3->get('DB')->exec($sql, $vars);

	$sql = "DELETE FROM AccountPerson WHERE person_id = :person_id AND account_id = :account_id";
	$vars = array(':account_id' => htmlspecialchars($POST['account_id']),
							 ':person_id' => $person_id);
	$f3->get('DB')->exec($sql, $vars);

	$sql = "INSERT INTO AccountPerson (account_id, person_id) VALUES (:account_id, :person_id)";
	$vars = array(':account_id' => htmlspecialchars($POST['account_id']),
							 ':person_id' => $person_id);
	$f3->get('DB')->exec($sql, $vars);

	$f3->reroute('/people');
}