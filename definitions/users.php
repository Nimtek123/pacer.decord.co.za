<?php

function blocked() {
	global $f3;
    $f3->set('content','blocked.htm');
	echo Template::instance()->render('layout.htm');
}

function loginGet() {
	global $f3;
	//print_r($f3->get('SESSION'));exit;
    /*
     * If user is already logged in, sending them away!
     */
    if (isLoggedIn()) {
        $f3->reroute('/');
    }

    /*
     * Empty any session data, just in case
     */
    $f3->clear('SESSION.user');

	$f3->set('content','login.htm');
	echo Template::instance()->render('layout.htm');
}

function loginPost() {
	global $f3;

    if (!$f3->exists('POST.login')) {
    	$f3->reroute('/');
    }

    /*
     * Check valid username and password
     */
    authenticate();
}

function authenticate() {
    global $f3;
    $ip    = $_SERVER['REMOTE_ADDR'];
    $email = $f3->get('POST.user_name');

    $created = time() - (3600 * 12);
    $sql     = "DELETE FROM Flood WHERE email = :email AND created < :created";
    $f3->get('DB')->exec($sql, array(':email' => $email, ':created' => $created));

    $sql          = "SELECT * FROM Flood WHERE email = :email";
    $emailresults = $f3->get('DB')->exec($sql, array(':email' => $email));

    if (count($emailresults) <= 3) {
        $mapper = new DB\SQL\Mapper($f3->get('DB'), 'Users');
        $auth   = new Auth($mapper, array('id' => 'email', 'pw' => 'pass'));

        $pass = hashPassword($f3->get('POST.user_password'));

        if ($f3->get('POST.user_password') == '') {
            flood($email, $ip);
            $f3->set('SESSION.failedlogin', 1);
            $f3->reroute('/login');
        }

        if ($auth->login($email, $pass)) {
        	$sql  = "SELECT * FROM Users WHERE email = :email";
            $user = $f3->get('DB')->exec($sql, array(':email' => $email));
            $f3->set('SESSION.user', $user);
            $f3->set('SESSION.instance', $_SERVER['HTTP_HOST']);
            $sql  = "DELETE FROM Flood WHERE email = :email";
            $f3->get('DB')->exec($sql, array(':email' => $email));
            $f3->reroute('/');
        }
        else {
            flood($email, $ip);
            $f3->set('SESSION.failedlogin', 1);
            $f3->reroute('/login');
        }
    }
    else {
        $f3->reroute('/blocked');
    }
}

function flood($email, $ip) {
    global $f3;
    $sql  = "INSERT INTO Flood VALUES (null, :ip, :time, :email)";
    $time = time();
    $f3->get('DB')->exec($sql, array(':ip' => $ip, ':time' => $time, ':email' => $email));
}

function unflood($email) {
    global $f3;
    $sql  = "DELETE FROM Flood WHERE email = :email";
    $time = time();
    $f3->get('DB')->exec($sql, array(':email' => $email));


    /*
     * Remove all the SecFlood data. If we don't we end up with problems after
     * the account has been reset - still get blocked at the drop of a hat.
     * Assuming the threat is neutralised after password reset...
     */
    $sql  = "DELETE FROM FloodSecurity WHERE floodsec_email = :floodsec_email";
    $vars = array(
        ':floodsec_email' => $email,
    );
    $f3->get('DB')->exec($sql, $vars);
}

function logoutGet() {
    global $f3;
    global $sessionHandler;

    $sessionHandler->removeAllUserSessions($f3->get('SESSION.user.0.user_id'));
    
    $f3->reroute('/login');
}

function doorman($lock = false, $value = false) {
	global $f3;
	$allow = true;
	if (!isLoggedIn()) $allow = false;
    if ($f3->exists('GET.key_hash') && $lock) {
        $sql  = "SELECT * FROM Locker WHERE key_hash = :key";
        $result = $f3->get('DB')->exec($sql, array(':key' => $f3->get('GET.key_hash')));
        if (isset($result[0]['expires']) && time() < $result[0]['expires'] && $result[0]['lock'] == $lock && $value == $result[0]['value']) {
            $allow = true;
        }
    }
    
    if (!$allow) $f3->reroute('/login'); //$f3->error(403);
}

function isLoggedIn() {
	global $f3;
	if (!$f3->exists('SESSION.user.0.user_id')) return false;
	else return true;
}

function issueKey() {
    global $f3;
    $lock = $f3->get('GET.lock');
    $value = $f3->get('GET.value');
    $email = $f3->get('GET.email');

    $f3->set('content','issue_key.htm');
    echo Template::instance()->render('layout.htm');
}

function emailIssuedKey() {
    global $f3;
    $lock = $f3->get('POST.lock');
    $value = $f3->get('POST.key');
    $email = $f3->get('POST.email');

    $expires = time() + (3600*48);
    $key_hash = md5(time().siteSalt());
    $subject = '';
    switch ($lock) {
        case 'job_id':
            $subject = 'Pacer: Approval Required';
            $link = 'http://'. $_SERVER['HTTP_HOST'] .'/job/'. $value .'/print?key_hash='. $key_hash;
            $sql = "DELETE FROM Locker WHERE `lock` = 'job_id' AND `value` = :job_id";
            $vars = array(':job_id' => intval($value));
            $f3->get('DB')->exec($sql, $vars);
            $message = 'Hi'."\n\n".'Please approve following this link: '.$link.' to approve your order. Once we have your approval we can proceed. This key will allow you access until '. date('H:i d-m-y', $expires) .'.'."\n\n".'Best regards,'."\n\n".'The Pacer Team';
            break;
        case 'account_id':
            $subject = 'Pacer: Account Status';
            $link = 'http://'. $_SERVER['HTTP_HOST'] .'/my-jobs/'. $value .'?key_hash='. $key_hash;
            $sql = "DELETE FROM Locker WHERE `lock` = 'account_id' AND `value` = :account_id";
            $vars = array(':account_id' => intval($value));
            $f3->get('DB')->exec($sql, $vars);
            $message = 'Hi'."\n\n".'Please find your account status by following this link: '.$link.'. This key will allow you access until '. date('H:i d-m-y', $expires) .'.'."\n\n".'Best regards,'."\n\n".'The Pacer Team';
            break;
    }

    $sql  = "INSERT INTO Locker VALUES (:key_hash, :value, :expires, :lock, :email)";
    $f3->get('DB')->exec($sql, array(':key_hash' => $key_hash, ':value' => $value, ':expires' => $expires, ':lock' => $lock, ':email' => $email));

    
    //$email = 'info@axisinteractive.co.za';
    
    $variables = array(
        'to' => array($email),
        'cc' => array(),
        'bcc' => array(),
        'subject' => $subject,
        'body' => $message,
        'html' => FALSE
    );
    emailer($variables);

    $f3->reroute('/');
}

