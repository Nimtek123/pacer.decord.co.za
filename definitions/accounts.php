<?php

function accountGet() {
	global $f3;
	$sql = "SELECT a.account_name, a.address, a.vat_number as \"VAT_no\" FROM Account a WHERE a.account_id = :account_id";
	$account_id = $f3->get('PARAMS.account_id');
	$vars = array(':account_id' => $account_id);
	$result = $f3->get('DB')->exec($sql, $vars);
	$account_name = $result[0]['account_name'];
	$account_vars = $result[0];
	
	$sql = "SELECT p.* FROM AccountPerson ap LEFT JOIN People p ON ap.person_id = p.person_id WHERE ap.account_id = :account_id";
	$vars = array(':account_id' => $account_id);
	$result = $f3->get('DB')->exec($sql, $vars);
	$people = array();
	foreach ($result as $row) {
		$people[] = $row;
	}
	$f3->set('account_id', $account_id);
	$f3->set('account_vars', $account_vars);
	$f3->set('account_name', $account_name);
	$f3->set('people', $people);
	$f3->set('content','account.htm');
	echo Template::instance()->render('layout.htm');
}

function accounts() {
	global $f3;
	if (isset($_GET['aname'])) {
	 $fname = htmlspecialchars($_GET['aname']);
	 $sql = "SELECT a.* FROM Account a WHERE a.account_name LIKE '%$fname%' ORDER BY a.account_name";
	}
	else $sql = "SELECT a.* FROM Account a order by a.account_name";
	
	$account_id = $f3->get('PARAMS.account_id');
	$result = $f3->get('DB')->exec($sql);
	$accounts = array();
	foreach ($result as $row) {
		$accounts[] = $row;
	}
	
	$f3->set('accounts', $accounts);
	$f3->set('content','accounts.htm');
	echo Template::instance()->render('layout.htm');
}

function accountAddGet() {
	global $f3;
	$f3->set('content','add_account.htm');
	echo Template::instance()->render('layout.htm');
}

function accountEditGetPost() {
	global $f3;
	$account_id = $f3->get('PARAMS.aid');
	$f3->set('account_id', $account_id);
	/* if vars are posted, use posted values. Else, look up values in DB.
	 * Either way, vars must be placed in universal variables container,
	 * so that form is oblivious as to how it gets values, provided that it gets them.
	 */
	$send_vars = array();
	$posted = $f3->get('POST');
	if (count($posted) > 0) {
		$send_vars = $posted;
		//perform error checking for account. If no errors, save and redirect
		$errors = array();
		if (strlen($posted['account_name']) < 5) {
			$errors['account_name'] = TRUE;
			}
		if (strlen($posted['account_address']) < 10) {
			$errors['account_address'] = TRUE;
		}
		if (count($errors) < 1) {
			$sql = "UPDATE Account SET address = :addr, account_name = :name, vat_number = :vat WHERE account_id = :aid";
			$vars = array(':addr' => $posted['account_address'], ':name' => $posted['account_name'], ':vat' => $posted['account_vat_number'], ':aid' => $account_id);
			$result = $f3->get('DB')->exec($sql, $vars);
			$f3->reroute('/account/'. $account_id);
		}
	}
	else {
		$sql = "SELECT a.account_name, a.address as \"account_address\", a.vat_number as \"account_vat_number\" FROM Account a WHERE a.account_id = :account_id";
		$vars = array(':account_id' => $account_id);
		$result = $f3->get('DB')->exec($sql, $vars);
		//$account_name = $result[0]['account_name'];
		$account_vars = $result[0];
		// do stuff with $account_vars so that they can be passed to $vars
		$send_vars = $account_vars;
	}
	// people are always the same
	$sql = "SELECT p.* FROM AccountPerson ap LEFT JOIN People p ON ap.person_id = p.person_id WHERE ap.account_id = :account_id";
	$vars = array(':account_id' => $account_id);
	$result = $f3->get('DB')->exec($sql, $vars);
	$people = array();
	foreach ($result as $row) {
		$people[] = $row;
	}
	$f3->set('account_id', $account_id);
	//$f3->set('account_vars', $account_vars);
	//$f3->set('account_name', $account_name);
	$f3->set('people', $people);
	$f3->set('form_values', $send_vars);
	$f3->set('content','edit_account.htm');
	echo Template::instance()->render('layout.htm');
}

function accountAddPost() {
	global $f3;
	$values = $f3->get('POST');
	//print_r($values);
	$errors = array();
	if (strlen($values['account_name']) < 5) {
		$errors['account_name'] = TRUE;
		}
	if (strlen($values['account_address']) < 10) {
		$errors['account_address'] = TRUE;
	}
	if (!strlen($values['person_first_name']) && !strlen($values['person_last_name'])) {
		$errors['person_first_name'] = TRUE;
	}
	if ((strlen($values['person_email']) < 1) && (strlen('person_phone1') < 1)) {
		$errors['person_email'] = TRUE;
	}
	else if (strlen($values['person_email']) > 1) {
		if (!filter_var($values['person_email'], FILTER_VALIDATE_EMAIL)) {
			$errors['person_email'] = TRUE;
		}
	}
	//print("Error count: ". count($errors));
	if (count($errors) < 1) { // no errors
		// save entered data: account, person, account_person
		$pa_count = 0;
		$f3->get('DB')->begin();
		$sql = "INSERT INTO Account (account_name, address, vat_number) VALUES (:aname, :aaddr, :vat)";
		$vars = array(':aname' => $f3->get('POST.account_name'), ':aaddr' => $f3->get('POST.account_address'), ':vat' => $f3->get('POST.account_vat_number'));
		$result = $f3->get('DB')->exec($sql, $vars);
		$account_id_raw = $f3->get('DB')->exec("SELECT MAX(account_id) as \"account_id\" FROM Account");
		$account_id = (count($account_id_raw) && is_array($account_id_raw[0]) && is_numeric($account_id_raw[0]['account_id'])) ? intval($account_id_raw[0]['account_id']): NULL;
		if ($account_id > 0) { // make a person and link to the account
			$sql = "INSERT INTO People (person_first_name, person_last_name, person_email, person_phone_1, person_phone_2, id_number)
				VALUES (:fname, :lname, :mail, :phone1, :phone2, :idnum)";
			$vars = array(
				':fname' => $f3->get('POST.person_first_name'), ':lname' => $f3->get('POST.person_last_name'), ':mail' => $f3->get('POST.person_email'),
				':phone1' => $f3->get('POST.person_phone1'), ':phone2' => $f3->get('POST.person_phone2'), ':idnum' => $f3->get('POST.person_id_number')
			);
			$result = $f3->get('DB')->exec($sql, $vars);
			$person_id_raw = $f3->get('DB')->exec("SELECT MAX(person_id) as \"person_id\" FROM People");
			$person_id = (count($person_id_raw) && is_array($person_id_raw[0]) && is_numeric($person_id_raw[0]['person_id']))
				? intval($person_id_raw[0]['person_id']): NULL;
			if ($person_id > 0) {
				$sql = "INSERT INTO AccountPerson (account_id, person_id) VALUES (:aid, :pid)";
				$vars = array(':aid' => $account_id, ':pid' => $person_id);
				$result = $f3->get('DB')->exec($sql, $vars);
				$pa_count_raw = $f3->get('DB')->exec(
					"SELECT count(account_id) as \"ap_count\" FROM AccountPerson WHERE account_id = :acid AND person_id = :prid",
					array(':acid' => $account_id, ':prid' => $person_id)
				);
				$pa_count = (count($pa_count_raw) && is_array($pa_count_raw[0]) && is_numeric($pa_count_raw[0]['ap_count']))
					? intval($pa_count_raw[0]['ap_count']): NULL;
			}
		}
		$f3->get('DB')->commit();
		if ($pa_count > 0) {
			// redirect to accounts page
			$f3->set('SESSION.message', 'Account created for '. htmlspecialchars($values['account_name'], ENT_QUOTES, 'UTF-8'));
			$f3->reroute('/accounts');
		}
		else { $f3->set('SESSION.error', 'Account creation failed for '. htmlspecialchars($values['account_name'], ENT_QUOTES, 'UTF-8') .':<br />'. $f3->get('DB')->log()); }
	} // no errors
	$f3->set('content','add_account.htm');
	echo Template::instance()->render('layout.htm');
}
 
