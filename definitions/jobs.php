<?php

function jobs() {
	global $f3;
	$f3->set('ESCAPE', FALSE);
	if (isset($_GET['j']) && isset($_GET['s']) && isset($_GET['t'])) {
		$job_id = $_GET['j'];
		$status_id = $_GET['s'];
		$time = $_GET['t'];
		$now = time();
		if (1) {
			if ($status_id == 4) {
				$sql = "UPDATE Job SET status_id = :status_id, delivered = :delivered WHERE job_id = :job_id";
				$vars = array(':status_id' => $status_id,':job_id' => $job_id, ':delivered' => $now);
			}
			elseif ($status_id !== 4 && isset($_GET['o']) && ($_GET['o'] == 'd' || $_GET['o'] == 'c')) {
				$sql = "UPDATE Job SET status_id = :status_id, delivered = null, closed = null WHERE job_id = :job_id";
				$vars = array(':status_id' => $status_id,':job_id' => $job_id);
			}
			else {
				$sql = "UPDATE Job SET status_id = :status_id WHERE job_id = :job_id";
				$vars = array(':status_id' => $status_id,':job_id' => $job_id);
			}
			
			$f3->get('DB')->exec($sql, $vars);
		}
	}
	
	$title = 'Jobs';
	$origin = 'j';
	$closed = 'j.closed IS NULL';
	$delivered = 'j.delivered IS NULL';
	$daterange = '';
	$account = '';
	$datefiltertext = '';
	$like = '';
	$job_messages = array();
	
	if (isset($_GET['v']) && $_GET['v'] == 'd') {
		$delivered = 'j.delivered IS NOT NULL';
		$title = 'Delivered Jobs';
		$origin = 'd';
		if (!isset($_GET['ds']) AND !isset($_GET['df']) AND !isset($_GET['djname'])) $_GET['ds'] = time() - (3600*24*5);
	}
	
	if (isset($_GET['v']) && $_GET['v'] == 'c') {
		$closed = 'j.closed IS NOT NULL';
		$title = 'Cancelled Jobs';
		$origin = 'c';
	}
	
	if (isset($_GET['df']) && preg_match('|(?mi-Us)^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$|', $_GET['df']) 
		&& isset($_GET['ds']) && preg_match('|(?mi-Us)^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$|', $_GET['ds']) ) {
		$daterange = ' AND j.created BETWEEN '. strtotime($_GET['ds']) .' AND '. strtotime($_GET['df']);
		$datefiltertext = 'Jobs created between '. date('H:i d/m/Y', strtotime($_GET['ds'])) .' and '. date('H:i d/m/Y', strtotime($_GET['df']));
	}
	elseif (isset($_GET['ds']) && preg_match('|(?mi-Us)^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$|', $_GET['ds']) ) {
		$daterange = ' AND j.created >= '. strtotime($_GET['ds']);
		$datefiltertext = 'Jobs created after '. date('H:i d/m/Y', strtotime($_GET['ds']));
	}
	elseif (isset($_GET['df']) && preg_match('|(?mi-Us)^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$|', $_GET['df']) ) {
		$daterange = ' AND j.created <= '. strtotime($_GET['df']);
		$datefiltertext = 'Jobs created before '. date('H:i d/m/Y', strtotime($_GET['df']));
	}
	
	if (isset($_GET['a']) && is_numeric($_GET['a'])) {
		$account = ' AND j.account_id = '. htmlspecialchars($_GET['a']);
		$title = 'Account-spcific Jobs';
		if (isset($_GET['v']) && $_GET['v'] == 'c') {
			$title = 'Cancelled Account-specific Jobs';
		}
		if (isset($_GET['v']) && $_GET['v'] == 'd') {
			$title = 'Delivered Account-specific Jobs';
		}
	}
	
	if (isset($_GET['djname'])) {
		$like = " AND j.description LIKE '%". htmlspecialchars($_GET['djname']) ."%'";
	}

	if (isset($_GET['status'])) {
		$like = " AND s.status_id = ". htmlspecialchars($_GET['status']) ."";
	}
	
	$sql = "SELECT j.*, a.account_name, person_first_name, p.person_last_name, p.person_email, p.person_phone_1, s.status FROM Job j
					LEFT JOIN Account a ON a.account_id = j.account_id
					LEFT JOIN People p ON p.person_id = j.person_id
					LEFT JOIN Status s ON j.status_id = s.status_id
					WHERE $closed AND $delivered". $daterange . $account . $like ." ORDER BY s.weight ASC, j.description";
	$result = $f3->get('DB')->exec($sql);
	$jobs = array();
	$jstatuses = array();
	foreach ($result as $row) {
		$row['lock'] = 'job_id';
		$row['key'] = $row['job_id'];
		$row['email'] = $row['person_email'];
		$jobs[] = $row;
		if (array_key_exists($row['status_id'], $jstatuses)) $jstatuses[$row['status_id']] += 1;
		else $jstatuses[$row['status_id']] = 1;
	}
	
	$sql = "SELECT status_id, status FROM Status ORDER BY weight ASC";
	$results = $f3->get('DB')->exec($sql);
	$statuses = array();
	foreach ($results as $row) {
		$statuses[$row['status_id']] = $row['status'];
	}
	
	$status_output = array();
	foreach ($jstatuses as $k => $v) {
		if (array_key_exists($k, $statuses)) {
			$status_output[] = '<a href="/?status='. $k .'" class="btn btn-xs status-'. $k .' status-counts">'. $statuses[$k] .' = '. $v .'</a>';
		}
		else {
			$status_output[] = '<a href="/?status=1" class="btn btn-xs status-1 status-counts">Error in status</a>';
		}
	}

	$statuscounts = '<b>Status Counts (Total '. array_sum($jstatuses) .'):</b> <a href="/" class="btn btn-xs status-counts-all">All</a> '. implode(' ', $status_output);

	$f3->set('statuscounts',$statuscounts);
	$f3->set('statuses',$statuses);
	$f3->set('datetext', $datefiltertext);
	$f3->set('yesrange', 'ds='. strtotime('yesterday') .'&df='. strtotime('today'));
	$f3->set('title', $title);
	$f3->set('pageclass', 'jobs');
	$f3->set('origin', $origin);
	$f3->set('jobs', $jobs);
	$f3->set('content','welcome.htm');
	echo Template::instance()->render('layout.htm');
}

function myJobs() {
	global $f3;
	$f3->set('ESCAPE', FALSE);
	
	$title = 'Jobs';
	$origin = 'j';
	$closed = 'j.closed IS NULL';
	$delivered = 'j.delivered IS NULL';
	$daterange = '';
	$account = '';
	$datefiltertext = '';
	$like = '';
	$job_messages = array();
	
	if (isset($_GET['v']) && $_GET['v'] == 'd') {
		$delivered = 'j.delivered IS NOT NULL';
		$title = 'Delivered Jobs';
		$origin = 'd';
		if (!isset($_GET['ds']) AND !isset($_GET['df']) AND !isset($_GET['djname'])) $_GET['ds'] = time() - (3600*24*5);
	}
	
	if (isset($_GET['v']) && $_GET['v'] == 'c') {
		$closed = 'j.closed IS NOT NULL';
		$title = 'Cancelled Jobs';
		$origin = 'c';
	}
	
	if (isset($_GET['df']) && preg_match('|(?mi-Us)^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$|', $_GET['df']) 
		&& isset($_GET['ds']) && preg_match('|(?mi-Us)^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$|', $_GET['ds']) ) {
		$daterange = ' AND j.created BETWEEN '. strtotime($_GET['ds']) .' AND '. strtotime($_GET['df']);
		$datefiltertext = 'Jobs created between '. date('H:i d/m/Y', strtotime($_GET['ds'])) .' and '. date('H:i d/m/Y', strtotime($_GET['df']));
	}
	elseif (isset($_GET['ds']) && preg_match('|(?mi-Us)^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$|', $_GET['ds']) ) {
		$daterange = ' AND j.created >= '. strtotime($_GET['ds']);
		$datefiltertext = 'Jobs created after '. date('H:i d/m/Y', strtotime($_GET['ds']));
	}
	elseif (isset($_GET['df']) && preg_match('|(?mi-Us)^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$|', $_GET['df']) ) {
		$daterange = ' AND j.created <= '. strtotime($_GET['df']);
		$datefiltertext = 'Jobs created before '. date('H:i d/m/Y', strtotime($_GET['df']));
	}
	
	if ($f3->exists('PARAMS.aid') && is_numeric($f3->get('PARAMS.aid'))) {
		$account = ' AND j.account_id = '. htmlspecialchars($f3->get('PARAMS.aid'));
	}
	else {
		$f3->reroute('/login');
	}
	
	if (isset($_GET['djname'])) {
		$like = " AND j.description LIKE '%". htmlspecialchars($_GET['djname']) ."%'";
	}

	if (isset($_GET['status'])) {
		$like = " AND s.status_id = ". htmlspecialchars($_GET['status']) ."";
	}
	
	$sql = "SELECT j.*, a.account_name, person_first_name, p.person_last_name, p.person_email, p.person_phone_1, s.status FROM Job j
					LEFT JOIN Account a ON a.account_id = j.account_id
					LEFT JOIN People p ON p.person_id = j.person_id
					LEFT JOIN Status s ON j.status_id = s.status_id
					WHERE $closed AND $delivered". $daterange . $account . $like ." ORDER BY s.weight ASC, j.description";
	$result = $f3->get('DB')->exec($sql);
	$jobs = array();
	$jstatuses = array();
	$jobspecific = array();
	foreach ($result as $row) {
		$title = $row['account_name'];

		$row['lock'] = 'job_id';
		$row['key'] = $row['job_id'];
		$row['email'] = $row['person_email'];
		$jobs[] = $row;
		if (array_key_exists($row['status_id'], $jstatuses)) $jstatuses[$row['status_id']] += 1;
		else $jstatuses[$row['status_id']] = 1;
	}

	$jobspecific = array('name' => $title, 'account_id' => $f3->get('PARAMS.aid'));
	
	$sql = "SELECT status_id, status FROM Status ORDER BY weight ASC";
	$results = $f3->get('DB')->exec($sql);
	$statuses = array();
	foreach ($results as $row) {
		$statuses[$row['status_id']] = $row['status'];
	}
	
	$status_output = array();
	foreach ($jstatuses as $k => $v) {
		$status_output[] = '<a href="/my-jobs/'. $f3->get('PARAMS.aid') .'?status='. $k .'&key_hash='. $f3->get('GET.key_hash') .'" class="btn btn-xs status-'. $k .' status-counts">'. $statuses[$k] .' = '. $v .'</a>';
	}

	$statuscounts = '<b>Status Counts (Total '. array_sum($jstatuses) .'):</b> <a href="/my-jobs/'. $f3->get('PARAMS.aid') .'?key_hash='. $f3->get('GET.key_hash') .'" class="btn btn-xs status-counts-all">All</a> '. implode(' ', $status_output);

	$f3->set('statuscounts',$statuscounts);
	$f3->set('statuses',$statuses);
	$f3->set('datetext', $datefiltertext);
	$f3->set('yesrange', 'ds='. strtotime('yesterday') .'&df='. strtotime('today'));
	$f3->set('title', $title);
	$f3->set('pageclass', 'jobs');
	$f3->set('origin', $origin);
	$f3->set('jobs', $jobs);
	$f3->set('jobspecific', $jobspecific);
	$f3->set('content','myjobs.htm');
	echo Template::instance()->render('layout.htm');
}

function jobsPrint() {
	global $f3;
	$job_id = $f3->get('PARAMS.jid');
	$sql = "SELECT j.*, a.account_name, p.person_first_name, p.person_last_name,
			p.person_email, p.person_phone_1, p.person_phone_2 FROM Job j
		LEFT JOIN Account a ON a.account_id = j.account_id
		LEFT JOIN People p ON p.person_id = j.person_id
		WHERE job_id = :job_id";
	$vars = array(':job_id' => $job_id);
	$results = $f3->get('DB')->exec($sql, $vars);
	$job_base = $results[0];
	
	$sql = "SELECT co.*, c.* FROM CapOrders co LEFT JOIN Caps c ON co.cap_id = c.cap_id WHERE co.job_id = :job_id";
	$vars = array(':job_id' => $job_id);
	$results = $f3->get('DB')->exec($sql, $vars);
	$cap_orders = array();
	foreach ($results as $row) {
		$cap_orders[$row['cap_order_id']] = $row;
	}
	
	$sql = "SELECT go.*, g.* FROM GoggleOrders go LEFT JOIN Goggles g ON g.goggle_id = go.goggle_id WHERE go.job_id = :job_id";
	$vars = array(':job_id' => $job_id);
	$results = $f3->get('DB')->exec($sql, $vars);
	$goggle_orders = array();
	foreach ($results as $row) {
		$goggle_orders[$row['goggle_order_id']] = $row;
	}
        
        $sql = "SELECT ao.*, a.* FROM AccessoryOrders ao LEFT JOIN Accessories a ON a.accessory_id = ao.accessory_id WHERE ao.job_id = :job_id";
	$vars = array(':job_id' => $job_id);
	$results = $f3->get('DB')->exec($sql, $vars);
	$accessory_orders = array();
	foreach ($results as $row) {
		$accessory_orders[$row['accessory_order_id']] = $row;
	}
	
	$sql = "SELECT status_id, status FROM Status ORDER BY status ASC";
	$results = $f3->get('DB')->exec($sql);
	$statuses = array();
	foreach ($results as $row) {
		$statuses[$row['status_id']] = $row['status'];
	}
	
	$f3->set('job', $job_base);
	$f3->set('caps', $cap_orders);
	$f3->set('statuses',$statuses);
	$f3->set('goggles', $goggle_orders);
        $f3->set('accessories', $accessory_orders);
	$f3->set('content','print_job.htm');
	echo Template::instance()->render('layout.htm');
}

function jobGet() {
	global $f3;
	$job_id = $f3->get('PARAMS.jid');
	$title = 'Job';
	
	$sql = "SELECT a.account_name, a.account_id FROM Account a";
	$results = $f3->get('DB')->exec($sql);
	$accounts = array();
	foreach ($results as $row) {
		$accounts[$row['account_id']] = $row['account_name'];
	}
	
	$sql = "SELECT CONCAT_WS(' ', p.person_first_name, p.person_last_name) as name, p.person_id FROM People p";
	$results = $f3->get('DB')->exec($sql);
	$people = array();
	foreach ($results as $row) {
		$people[$row['person_id']] = $row['name'];
	}
	
	$sql = "SELECT cap_id, cap_description, product_code FROM Caps ORDER BY cap_description ASC";
	$results = $f3->get('DB')->exec($sql);
	$caps = array();
	foreach ($results as $row) {
		$caps[$row['cap_id']] = $row['cap_description'] . ' ('. $row['product_code'] .')';
	}
	
	$sql = "SELECT goggle_id, goggle_description FROM Goggles ORDER BY goggle_description ASC";
	$results = $f3->get('DB')->exec($sql);
	$goggles = array();
	foreach ($results as $row) {
		$goggles[$row['goggle_id']] = $row['goggle_description'];
	}
        
        $sql = "SELECT accessory_id, accessory_description FROM Accessories ORDER BY accessory_description ASC";
	$results = $f3->get('DB')->exec($sql);
	$accessories = array();
	foreach ($results as $row) {
		$accessories[$row['accessory_id']] = $row['accessory_description'];
	}
	
	$sql = "SELECT status_id, status FROM Status ORDER BY weight ASC";
	$results = $f3->get('DB')->exec($sql);
	$statuses = array();
	foreach ($results as $row) {
		$statuses[$row['status_id']] = $row['status'];
	}
	
	if (is_numeric($job_id)) {
		$sql = "SELECT j.* FROM Job j WHERE j.job_id = :job_id";
		$vars = array(':job_id' => $job_id);
		$results = $f3->get('DB')->exec($sql, $vars);
		if ($results) $job_base =$results[0];
		else $f3->reroute('/');
		
		$sql = "SELECT co.* FROM CapOrders co WHERE co.job_id = :job_id";
		$vars = array(':job_id' => $job_id);
		$results = $f3->get('DB')->exec($sql, $vars);
		$cap_orders = array();
		foreach ($results as $row) {
			$cap_orders[$row['cap_order_id']] = $row;
		}
		
		$sql = "SELECT co.* FROM GoggleOrders co WHERE co.job_id = :job_id";
		$vars = array(':job_id' => $job_id);
		$results = $f3->get('DB')->exec($sql, $vars);
		$goggle_orders = array();
		foreach ($results as $row) {
			$goggle_orders[$row['goggle_order_id']] = $row;
		}
                
                $sql = "SELECT co.* FROM AccessoryOrders co WHERE co.job_id = :job_id";
		$vars = array(':job_id' => $job_id);
		$results = $f3->get('DB')->exec($sql, $vars);
		$accessory_orders = array();
		foreach ($results as $row) {
			$accessory_orders[$row['accessory_order_id']] = $row;
		}
	}
	elseif ($job_id == 'new') {
	    $f3->set('cap_order_id', 0);
	}
	else $f3->error(403);

	$f3->set('job_base',isset($job_base) ? $job_base : null);
	$f3->set('cap_orders',isset($cap_orders) ? $cap_orders : null);
	$f3->set('goggle_orders',isset($goggle_orders) ? $goggle_orders : null);
        $f3->set('accessory_orders',isset($accessory_orders) ? $accessory_orders : null);
	$f3->set('title',$title);
	$f3->set('accounts',$accounts);
	$f3->set('people',$people);
	$f3->set('statuses',$statuses);
	$f3->set('caps',$caps);
	$f3->set('goggles',$goggles);
        $f3->set('accessories',$accessories);
	$f3->set('content','work_job.htm');
	echo Template::instance()->render('layout.htm');
}

function jobPost() {
	global $f3;
	$job_id = $f3->get('PARAMS.jid');
	$POST = $f3->get('POST');
	
	if (is_numeric($job_id)) {
		$sql = "UPDATE Job SET account_id = :account_id, description = :description, deadline = :deadline,
			person_id = :person_id, accessories = :accessories, closed = :closed,
			delivered = :delivered, note = :note, delivery_address = :delivery_address, status_id = :status_id, tracking_numbers = :tracking_numbers, approval_confirmed = :approval_confirmed
			WHERE job_id = :job_id";
			
		$status = $POST['status_id'];
		$delivered = strtotime($POST['delivered']);
		$cancelled = strtotime($POST['closed']);
		if ($status == 4 && empty($delivered) && empty($cancelled)) {
			$delivered = time();
		}
		elseif (!empty($delivered) && $status != 4 && empty($cancelled)) {
			$status = 4;
		}
		elseif (!empty($delivered) && $status == 4 && empty($cancelled)) {
			// do nothing
		}
		elseif (empty($delivered) && $status == 4 && !empty($cancelled)) {
			// do nothing
		}
		elseif (empty($delivered) && $status != 4 && !empty($cancelled)) {
			$status = 4;
		}
		elseif (!empty($delivered) && !empty($cancelled)) {
			$f3->reroute('/');
		}
		
		if ($delivered < 1) $delivered = null;
		if ($cancelled < 1) $cancelled = null;
		
		$vars = array(
			':account_id' => $POST['account_id'],
			':description' => $POST['description'],
			':deadline' => (!empty($POST['deadline']) ? strtotime($POST['deadline']) : null),
			':person_id' => $POST['person_id'],
			':accessories' => (isset($POST['accessories']) ? $POST['accessories'] : 0),
			':closed' => $cancelled,
			':delivered' => $delivered,
			':note' => $POST['note'],
			':delivery_address' => $POST['delivery_address'],
			':status_id' => $status,
			':tracking_numbers' => $POST['tracking_numbers'],
			':job_id' => $job_id,
			':approval_confirmed' => (isset($POST['approval_confirmed']) ? $POST['approval_confirmed'] : 0)
		);
		$f3->get('DB')->exec($sql, $vars);
                
		// all cap orders are new as new job
		foreach ($POST['cap_row'] as $cap_order_id => $order_data) {
			if ($order_data['cap_id'] && preg_match('/^n[0-9]*/', $cap_order_id)) { // new cap order
				$sql = "INSERT INTO CapOrders (job_id, cap_id, quantity, number_colours, colours, artwork_image, number_names, price)
						VALUES (:job_id, :cap_id, :quantity, :number_colours, :colours, :artwork_image, :number_names, :price)";
				$vars = array(
					':job_id' => $job_id,
					':cap_id' => $order_data['cap_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price'],
					':number_colours' => $order_data['number_colours'],
					':colours' => $order_data['colours'],
					':artwork_image' => isset($order_data['old_artwork_image']) ? $order_data['old_artwork_image'] : null,
					':number_names' => isset($order_data['number_names']) ? 1 : 0
				);
				$f3->get('DB')->exec($sql, $vars);
				
				$result = $f3->get('DB')->exec("SELECT MAX(cap_order_id) as \"cap_order_id\" FROM CapOrders");
				$this_cap_order_id = intval($result[0]['cap_order_id']);
				
				if (!empty($_FILES['cap_row']['name'][$cap_order_id]['artwork_image']) && !isset($order_data['delete_artwork_image'])) {
					$upload_dir = '/artwork/'. $job_id;
					$file_name = $job_id .'-'. $this_cap_order_id .'.jpg';
					
					if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $upload_dir) && !is_dir($_SERVER['DOCUMENT_ROOT'] . $upload_dir)) {
						mkdir($_SERVER['DOCUMENT_ROOT'] . $upload_dir);      
					} 
					
					
					$upload_path = $_SERVER['DOCUMENT_ROOT'] . $upload_dir .'/'. $file_name;
					move_uploaded_file($_FILES['cap_row']['tmp_name'][$cap_order_id]['artwork_image'], $upload_path);
					
					$sql = "UPDATE CapOrders SET artwork_image = :artwork_image WHERE cap_order_id = :cap_order_id";
					$vars = array(
						':cap_order_id' => $this_cap_order_id,
						':artwork_image' => $upload_dir .'/'. $file_name
					);
					$f3->get('DB')->exec($sql, $vars);
				}
			}
			elseif ($order_data['cap_id'] && preg_match('/^[0-9]*/', $cap_order_id)) { // existing cap order
				$sql = "UPDATE CapOrders SET job_id = :job_id, cap_id = :cap_id, quantity = :quantity, number_colours = :number_colours,
					colours = :colours, artwork_image = :artwork_image, final_image = :final_image, number_names = :number_names,
					price = :price
					WHERE cap_order_id = :cap_order_id";
				$vars = array(
					':job_id' => $job_id,
					':cap_id' => $order_data['cap_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price'],
					':number_colours' => $order_data['number_colours'],
					':colours' => $order_data['colours'],
					':artwork_image' => isset($order_data['old_artwork_image']) ? $order_data['old_artwork_image'] : null,
					':final_image' => isset($order_data['old_final_image']) ? $order_data['old_final_image'] : null,
					':number_names' => isset($order_data['number_names']) ? 1 : 0,
					':cap_order_id' => $cap_order_id
				);
				$f3->get('DB')->exec($sql, $vars);
				
				$this_cap_order_id = $cap_order_id;
				
				if ((isset($order_data['old_artwork_image']) && !empty($order_data['old_artwork_image'])) && isset($order_data['delete_artwork_image'])) {
					if (file_exists($_SERVER['DOCUMENT_ROOT'] . $order_data['old_artwork_image'])) {
						unlink($_SERVER['DOCUMENT_ROOT'] . $order_data['old_artwork_image']);
					}
					$sql = "UPDATE CapOrders SET artwork_image = :artwork_image WHERE cap_order_id = :cap_order_id";
					$vars = array(
						':cap_order_id' => $this_cap_order_id,
						':artwork_image' => null
					);
					$f3->get('DB')->exec($sql, $vars);
				}
				
				if ((isset($order_data['old_final_image']) && !empty($order_data['old_final_image'])) && isset($order_data['delete_final_image'])) {
					if (file_exists($_SERVER['DOCUMENT_ROOT'] . $order_data['old_final_image'])) {
						unlink($_SERVER['DOCUMENT_ROOT'] . $order_data['old_final_image']);
					}
					$sql = "UPDATE CapOrders SET final_image = :final_image WHERE cap_order_id = :cap_order_id";
					$vars = array(
						':cap_order_id' => $this_cap_order_id,
						':final_image' => null
					);
					$f3->get('DB')->exec($sql, $vars);
				}
				
				if (!empty($_FILES['cap_row']['name'][$cap_order_id]['artwork_image']) && (isset($order_data['delete_artwork_image']) ||
					(!isset($order_data['old_artwork_image']) || empty($order_data['old_artwork_image'])))) { // old image being deleted, and there is a new one
					$upload_dir = '/artwork/'. $job_id;
					$file_name = $job_id .'-'. $this_cap_order_id .'.jpg';
					
					if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $upload_dir) && !is_dir($_SERVER['DOCUMENT_ROOT'] . $upload_dir)) {
						mkdir($_SERVER['DOCUMENT_ROOT'] . $upload_dir);      
					} 
					
					
					$upload_path = $_SERVER['DOCUMENT_ROOT'] . $upload_dir .'/'. $file_name;
					move_uploaded_file($_FILES['cap_row']['tmp_name'][$cap_order_id]['artwork_image'], $upload_path);
					
					$sql = "UPDATE CapOrders SET artwork_image = :artwork_image WHERE cap_order_id = :cap_order_id";
					$vars = array(
						':cap_order_id' => $this_cap_order_id,
						':artwork_image' => $upload_dir .'/'. $file_name
					);
					$f3->get('DB')->exec($sql, $vars);
				}
				
				if (!empty($_FILES['cap_row']['name'][$cap_order_id]['final_image']) && (isset($order_data['delete_final_image']) ||
					(!isset($order_data['old_final_image']) || empty($order_data['old_final_image'])))) { // old image being deleted, and there is a new one
					$upload_dir = '/artwork/'. $job_id;
					$file_name = $job_id .'-'. $this_cap_order_id .'-final.jpg';
					
					if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $upload_dir) && !is_dir($_SERVER['DOCUMENT_ROOT'] . $upload_dir)) {
						mkdir($_SERVER['DOCUMENT_ROOT'] . $upload_dir);      
					} 
					
					
					$upload_path = $_SERVER['DOCUMENT_ROOT'] . $upload_dir .'/'. $file_name;
					move_uploaded_file($_FILES['cap_row']['tmp_name'][$cap_order_id]['final_image'], $upload_path);
					
					$sql = "UPDATE CapOrders SET final_image = :final_image WHERE cap_order_id = :cap_order_id";
					$vars = array(
						':cap_order_id' => $this_cap_order_id,
						':final_image' => $upload_dir .'/'. $file_name
					);
					$f3->get('DB')->exec($sql, $vars);
				}
			}
			elseif (!$order_data['cap_id'] && preg_match('/^[0-9]*/', $cap_order_id)) { // existing cap order deleted
				$sql = "DELETE FROM CapOrders WHERE cap_order_id = :cap_order_id";
				$vars = array(
					':cap_order_id' => $cap_order_id
				);
				$f3->get('DB')->exec($sql, $vars);
				
				$this_cap_order_id = $cap_order_id;
				
				if ((isset($order_data['old_artwork_image']) && !empty($order_data['old_artwork_image']))) {
					if (file_exists($_SERVER['DOCUMENT_ROOT'] . $order_data['old_artwork_image'])) {
						unlink($_SERVER['DOCUMENT_ROOT'] . $order_data['old_artwork_image']);
					}
				}
			}
		}
		
		// goggles
		foreach ($POST['goggles_row'] as $goggle_order_id => $order_data) {
			if ($order_data['goggle_id'] && preg_match('/^n[0-9]*/', $goggle_order_id)) { // new goggle order
				$sql = "INSERT INTO GoggleOrders (job_id, goggle_id, quantity, price)
						VALUES (:job_id, :goggle_id, :quantity, :price)";
				$vars = array(
					':job_id' => $job_id,
					':goggle_id' => $order_data['goggle_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price'],
				);
				$f3->get('DB')->exec($sql, $vars);
			}
			elseif ($order_data['goggle_id'] && preg_match('/^[0-9]*/', $goggle_order_id)) { // existing goggle order
				$sql = "UPDATE GoggleOrders SET job_id = :job_id, goggle_id = :goggle_id, quantity = :quantity, price = :price WHERE goggle_order_id = :goggle_order_id";
				$vars = array(
					':job_id' => $job_id,
					':goggle_id' => $order_data['goggle_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price'],
					':goggle_order_id' => $goggle_order_id
				);
				$f3->get('DB')->exec($sql, $vars);
			}
			elseif (!$order_data['goggle_id'] && preg_match('/^[0-9]*/', $goggle_order_id)) { // existing cap order deleted
				$sql = "DELETE FROM GoggleOrders WHERE goggle_order_id = :goggle_order_id";
				$vars = array(
					':goggle_order_id' => $goggle_order_id
				);
				$f3->get('DB')->exec($sql, $vars);
			}
		}
                
                // accessories
                foreach ($POST['accessories_row'] as $accessory_order_id => $order_data) {
			if ($order_data['accessory_id'] && preg_match('/^n[0-9]*/', $accessory_order_id)) { // new accessory order
				$sql = "INSERT INTO AccessoryOrders (job_id, accessory_id, quantity, price)
						VALUES (:job_id, :accessory_id, :quantity, :price)";
				$vars = array(
					':job_id' => $job_id,
					':accessory_id' => $order_data['accessory_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price'],
				);
				$f3->get('DB')->exec($sql, $vars);
			}
			elseif ($order_data['accessory_id'] && preg_match('/^[0-9]*/', $accessory_order_id)) { // existing accessory order
				$sql = "UPDATE AccessoryOrders SET job_id = :job_id, accessory_id = :accessory_id, quantity = :quantity, price = :price
                                    WHERE accessory_order_id = :accessory_order_id";
				$vars = array(
					':job_id' => $job_id,
					':accessory_id' => $order_data['accessory_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price'],
					':accessory_order_id' => $goggle_order_id
				);
				$f3->get('DB')->exec($sql, $vars);
			}
			elseif (!$order_data['accessory_id'] && preg_match('/^[0-9]*/', $accessory_order_id)) { // existing accessory order deleted
				$sql = "DELETE FROM AccessoryOrders WHERE accessory_order_id = :accessory_order_id";
				$vars = array(
					':accessory_order_id' => $accessory_order_id
				);
				$f3->get('DB')->exec($sql, $vars);
			}
		}
                
	}
	elseif ($job_id == 'new') {
		$sql = "INSERT INTO Job (account_id, description, deadline, person_id, accessories, created, closed, delivered, note, delivery_address, status_id, tracking_numbers)
		VALUES (:account_id, :description, :deadline, :person_id, :accessories, :created, :closed, :delivered, :note, :delivery_address, :status_id, :tracking_numbers)";
		$vars = array(
			':account_id' => $POST['account_id'],
			':description' => $POST['description'],
			':deadline' => (!empty($POST['deadline']) ? strtotime($POST['deadline']) : null),
			':person_id' => $POST['person_id'],
			':accessories' => (isset($POST['accessories']) ? $POST['accessories'] : 0),
			':created' => time(),
			':closed' => (isset($POST['closed']) && intval($POST['closed']) > 0 && empty($POST['delivered']) ? strtotime($POST['closed']) : null),
			':delivered' => (isset($POST['delivered']) && intval($POST['delivered']) > 0 ? strtotime($POST['delivered']) : null),
			':note' => $POST['note'],
			':status_id' => ((isset($POST['delivered']) && intval($POST['delivered']) > 0) || (isset($POST['closed']) && intval($POST['closed']) > 0) ? 4 : $POST['status_id']),
			':tracking_numbers' => $POST['tracking_numbers'],
			':delivery_address' => $POST['delivery_address']
		);
		$f3->get('DB')->exec($sql, $vars);
		
		$result = $f3->get('DB')->exec("SELECT MAX(job_id) as \"job_id\" FROM Job");
		$job_id = intval($result[0]['job_id']);
		
		// all cap orders are new as new job
		foreach ($POST['cap_row'] as $cap_order_id => $order_data) {
			if ($order_data['cap_id']) {
				$sql = "INSERT INTO CapOrders (job_id, cap_id, quantity, price, number_colours, colours, artwork_image, number_names)
						VALUES (:job_id, :cap_id, :quantity, :price, :number_colours, :colours, :artwork_image, :number_names)";
				$vars = array(
					':job_id' => $job_id,
					':cap_id' => $order_data['cap_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price'],
					':number_colours' => $order_data['number_colours'],
					':colours' => $order_data['colours'],
					':artwork_image' => isset($order_data['old_artwork_image']) ? $order_data['old_artwork_image'] : null,
					':number_names' => isset($order_data['number_names']) ? 1 : 0
				);
				$f3->get('DB')->exec($sql, $vars);
				
				$result = $f3->get('DB')->exec("SELECT MAX(cap_order_id) as \"cap_order_id\" FROM CapOrders");
				$this_cap_order_id = intval($result[0]['cap_order_id']);
				
				if (!empty($_FILES['cap_row']['name'][$cap_order_id]['artwork_image']) && !isset($order_data['delete_artwork_image'])) {
					$upload_dir = '/artwork/'. $job_id;
					$file_name = $job_id .'-'. $this_cap_order_id .'.jpg';
					
					if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $upload_dir) && !is_dir($_SERVER['DOCUMENT_ROOT'] . $upload_dir)) {
						mkdir($_SERVER['DOCUMENT_ROOT'] . $upload_dir);      
					} 
					
					
					$upload_path = $_SERVER['DOCUMENT_ROOT'] . $upload_dir .'/'. $file_name;
					move_uploaded_file($_FILES['cap_row']['tmp_name'][$cap_order_id]['artwork_image'], $upload_path);
					
					$sql = "UPDATE CapOrders SET artwork_image = :artwork_image WHERE cap_order_id = :cap_order_id";
					$vars = array(
						':cap_order_id' => $this_cap_order_id,
						':artwork_image' => $upload_dir .'/'. $file_name
					);
					$f3->get('DB')->exec($sql, $vars);
				}
			}
		}
		
		// goggles
		foreach ($POST['goggles_row'] as $goggle_order_id => $order_data) {
			if (!empty($order_data['goggle_id'])) {
				$sql = "INSERT INTO GoggleOrders (job_id, goggle_id, quantity, price)
						VALUES (:job_id, :goggle_id, :quantity, :price)";
				$vars = array(
					':job_id' => $job_id,
					':goggle_id' => $order_data['goggle_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price']
				);
				$f3->get('DB')->exec($sql, $vars);
			}
		}
                
                // accessories
                foreach ($POST['accessories_row'] as $accessory_order_id => $order_data) {
			if (!empty($order_data['accessory_id'])) {
				$sql = "INSERT INTO AccessoryOrders (job_id, accessory_id, quantity, price)
						VALUES (:job_id, :accessory_id, :quantity, :price)";
				$vars = array(
					':job_id' => $job_id,
					':accessory_id' => $order_data['accessory_id'],
					':quantity' => $order_data['quantity'],
					':price' => $order_data['price']
				);
				$f3->get('DB')->exec($sql, $vars);
			}
		}
	}
	
	$f3->reroute('/job/'. $job_id .'/print');
}

function approveJob() {
	global $f3;
	if ($f3->exists('PARAMS.jid') && is_numeric($f3->get('PARAMS.jid'))) {
		
		$sql  = "UPDATE `Job` SET `approval_confirmed` = :approved WHERE job_id = :job_id";
	    $f3->get('DB')->exec($sql, array(':approved' => 1, ':job_id' => intval($f3->get('PARAMS.jid'))));

		$sql  = "INSERT INTO `Approval` VALUES (NULL, :job_id, :approved)";
	    $f3->get('DB')->exec($sql, array(':job_id' => intval($f3->get('PARAMS.jid')), ':approved' => time()));
	    $f3->set('content','approved.htm');
	    $message = 'Job '. $f3->get('PARAMS.jid') .' has been approved';
	    
	    $variables = array(
	    	'to' => array('info@axisinteractive.co.za'),
	    	'cc' => array(),
	    	'bcc' => array(),
	    	'subject' => $message,
	    	'body' => $message,
	    	'html' => FALSE
	    );
	    emailer($variables);
	}
	else {
		$f3->set('content','404.htm');
	}

    echo Template::instance()->render('layout.htm');
}

