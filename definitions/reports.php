<?php

function reportBuilder($f3, $params) {
	$title = "Pacer reports";
  
  $report_id = check_plain($params['report_id']);
  
  $sql = "SELECT * FROM Report";
  $vars = array();
	$results = $f3->get('DB')->exec($sql, $vars);
  $reports = array();
	foreach ($results as $row) {
    $reports[] = $row;
    if ($report_id == $row['report_id']) {
      $title = $row['report_name'];
      $query = $row['report_query'];
    }
	}
  
  preg_match_all( '|(?mi-Us):var\\d+|', $query, $matches);
  $vars = array();
  foreach ($matches[0] as $v) {
    if (isset($_GET[$v])) {
      if ($_GET[$v] != 'null') {
        $vars[$v] = check_plain($_GET[$v]);
      }
      else {
        switch ($v) {
          case ':var1':
          case ':var2':
            $query = str_replace('ij.created BETWEEN :var1 AND :var2', '1', $query);
            $query = str_replace('j.created BETWEEN :var1 AND :var2', '1', $query);
            break;
          case ':var3':
            $query = str_replace('ij.account_id = :var3', '1', $query);
            $query = str_replace('j.account_id = :var3', '1', $query);
            break;
        }
      }
    }
    else $f3->reroute('/');
  }
  
  $f3->set('sql', $query);
  $f3->set('sqlvars', $vars);
  
  $results = $f3->get('DB')->exec($query, $vars);
  $output = array();
  $header = array();
  foreach ($results as $row) {
    $output[] = $row;
    $header = array_keys($row);
  }
  
  $f3->set('header', $header);
  $f3->set('output', $output);
  
  // controller
  $timedate = array(
    array(strtotime('00:00 first day of this month'), strtotime('00:00 first day of next month'), 'this month'),
    array(strtotime('00:00 first day of last month'), strtotime('00:00 first day of this month'), 'last month'),
    array(strtotime('00:00 first day of this year'), strtotime('00:00 first day of next year'), 'this year'),
    array(strtotime('00:00 first day of last year'), strtotime('00:00 first day of this year'), 'last year')
  );
  $f3->set('params', $params);
  $f3->set('gets', $_GET);
  $f3->set('reports', $reports);
  $f3->set('timedate', $timedate);
  $accounts = get_accounts();
  $f3->set('accounts', $accounts);
  
  $f3->set('title', $title);
	$f3->set('pageclass', 'reports');
	$f3->set('content','report.htm');
	echo Template::instance()->render('layout.htm');
}