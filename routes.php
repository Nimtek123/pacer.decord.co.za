<?php

$f3->route('GET /', function($f3) {doorman();jobs();});
$f3->route('GET /my-jobs/@aid', function($f3) {doorman('account_id', $f3->get('PARAMS.aid'));myJobs();});
$f3->route('GET /job/@jid/print', function($f3) {doorman('job_id', $f3->get('PARAMS.jid'));jobsPrint();});
$f3->route('GET /job/@jid', function($f3) {doorman();jobGet();});
$f3->route('POST /job/@jid', function($f3) {doorman();jobPost();});
$f3->route('GET /issuekey/@type', function($f3) {doorman();issueKey();});
$f3->route('POST /issuekey/@type', function($f3) {doorman();emailIssuedKey();});
$f3->route('GET /approve/@jid', function($f3) {approveJob();});

$f3->route('GET /account/@account_id', function($f3) {doorman();accountGet();});
$f3->route('GET /accounts', function($f3) {doorman();accounts();});
$f3->route('GET /accounts/add', function($f3) {doorman();accountAddGet();});
$f3->route('GET|POST /account/@aid/edit', function($f3) {doorman();accountEditGetPost();});
$f3->route('POST /accounts/add', function($f3) {doorman();accountAddPost();});

$f3->route('GET /people', function($f3) {doorman();people();});
$f3->route('GET /people/add', function($f3) {doorman();peopleAddGet();});
$f3->route('GET /people/add/@aid', function($f3) {doorman();peopleAddGet();});
$f3->route('POST /people/add', function($f3) {doorman();peopleAddPost();});
$f3->route('GET /people/staff', function($f3) {doorman();peopleStaff();});
$f3->route('GET /userref', function($f3) {doorman();userRef();});
$f3->route('GET /person/@pid/edit', function($f3) {doorman();personEditGet();});
$f3->route('POST /person/@pid/edit', function($f3) {doorman();personEditPost();});

$f3->route('GET /login', function($f3) {loginGet();});
$f3->route('POST /login', function($f3) {loginPost();});
$f3->route('GET /logout', function($f3) {logoutGet();});

$f3->route('GET /search', function($f3) {doorman();searchGet();});
$f3->route('POST /search', function($f3) {doorman();searchPost();});

/*
 * Reports
 */
$f3->route('GET|POST /report/jobs', function($f3) {doorman();report_jobs();});
$f3->route('GET /report/customer', function($f3) {doorman();report_customer();});

$f3->route('GET /blocked', 'blocked');

$f3->route('GET /reports/@report_id', function($f3, $params) {doorman();reportBuilder($f3, $params);});

 $f3->route('GET /ajax/@op/@id [ajax]', 'pacer_ajax', 1); // force to cache for only 1 second, as this can cause issues with lookup
 $f3->route('GET /ajax/@op/@id [sync]', 'pacer_ajax', 1);