<?php

// number of caps by type per account
$sql = "select c.cap_description as ITEM, SUM(co.quantity) as NUMBER, a.account_name as ACCOUNT from CapOrders co LEFT JOIN Caps c ON co.cap_id = c.cap_id LEFT JOIN Job j ON co.job_id = j.job_id LEFT JOIN Account a ON j.account_id = a.account_id where j.account_id = 1 group by co.cap_id order by NUMBER DESC";

// most proflific cap order accounts buying Pacer caps
$sql = "SELECT a.account_name AS Account, SUM(co.quantity) AS Value, ((SUM(co.quantity) / (SELECT SUM(quantity) FROM CapOrders))*100) AS Percent
        FROM CapOrders co
        LEFT JOIN Caps c ON co.cap_id = c.cap_id
        LEFT JOIN Job j ON co.job_id = j.job_id
        LEFT JOIN Account a ON j.account_id = a.account_id
        WHERE c.cap_description NOT LIKE '%spurt%'
        AND j.created BETWEEN 1451599200 AND 1472680800
        GROUP BY j.account_id
        ORDER BY Percent DESC";
        
// most proflific cap order accounts
$sql = "SELECT a.account_name AS Account, SUM(co.quantity) AS Value, ((SUM(co.quantity) / (SELECT SUM(quantity) FROM CapOrders))*100) AS Percent
        FROM CapOrders co
        LEFT JOIN Caps c ON co.cap_id = c.cap_id
        LEFT JOIN Job j ON co.job_id = j.job_id
        LEFT JOIN Account a ON j.account_id = a.account_id
        GROUP BY j.account_id
        ORDER BY Percent DESC";