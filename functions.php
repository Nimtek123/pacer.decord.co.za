<?php

function unix_to_days( $diff, $levels=2 ){  
    $times = array(31536000, 2628000, 604800, 86400, 3600, 60, 1);
    $words = array('yr', 'mth', 'wk', 'dy', 'hr', 'min', 'sec');
    $str = array();
    foreach ($times as $k=>$v){
        $val = floor($diff / $v);
        if ($val) {
            $str[] = $val .' '. $words[$k] . ($val==1 ? '' : 's');
            $levels--;
        }
        $diff %= $v;
        if ($levels==0) break;
    }
    $flat = implode(', ', $str);
    return $flat;
}

function siteSalt() {
	return 'h4gdet37dh589jh4567dh46h57';
}

function debug($var, $ws = true) {
	if ($ws) {
		print '<pre>';
		var_dump($var);
		print '</pre>';
		exit;
	}
	else {
		print '<pre>';
		var_dump($var);
		print '</pre>';
	}
}

function check_plain($text) {
  return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}

function hashPassword($password) {
    $crypt = \Bcrypt::instance();
    $salt  = siteSalt();
    $hash  = $crypt->hash($password, $salt);
    return $hash;
}

/* Returns a file size limit in bytes based on the PHP upload_max_filesize and post_max_size
 * @see http://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size
*/
function file_upload_max_size() {
  static $max_size = -1;

  if ($max_size < 0) {
    // Start with post_max_size.
    $max_size = parse_size(ini_get('post_max_size'));

    // If upload_max_size is less, then reduce. Except if upload_max_size is
    // zero, which indicates no limit.
    $upload_max = parse_size(ini_get('upload_max_filesize'));
    if ($upload_max > 0 && $upload_max < $max_size) {
      $max_size = $upload_max;
    }
  }
  return $max_size;
}

function parse_size($size) {
  $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
  $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
  if ($unit) {
    // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
    return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
  }
  else {
    return round($size);
  }
}

function get_accounts() {
	global $f3;
	$sql = "SELECT * FROM Account";
	$result = $f3->get('DB')->exec($sql);
	
	$accounts = array();
	foreach ($result as $row) {
		$accounts[$row['account_id']] = $row;
	}
	
	return $accounts;
}

// create a globally unique ID
function GUID () {
    if (function_exists('com_create_guid') === true)
    {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function message($input, $type = 'info') {
    global $f3;
    if (!in_array($type, array('warning', 'info', 'debug', 'goodthing'))) $type = 'info';
    
    if ($type == 'debug' && is_string($input)) {
        $input = htmlentities($input);
    }
    
    if ($f3->get('SESSION.messages') !== null) $f3->push('SESSION.messages', array('msg' => $input, 'type' => $type));
    else $f3->set('SESSION.messages', array(array('msg' => $input, 'type' => $type)));
}

function clean_up() {
    global $f3;
    $f3->clear('SESSION.messages');
    $f3->clear('SESSION.validate');
}

function emailer($variables) {
  require 'phpmailer/PHPMailerAutoload.php';

  $mail = new PHPMailer;

  //$mail->SMTPDebug = 3;                               // Enable verbose debug output

  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.decord.co.za';  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = 'no-reply@decord.co.za';                 // SMTP username
  $mail->Password = '123456Mailit';                           // SMTP password
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to

  $mail->SMTPOptions = array(
      'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false,
          'allow_self_signed' => true
      )
  );
  
  $mail->setFrom('no-reply@decord.co.za', 'Pacer');
  foreach ($variables['to'] as $em) {
    $mail->addAddress($em);
  }
  foreach ($variables['cc'] as $em) {
    $mail->addCC($em);
  }
  foreach ($variables['bcc'] as $em) {
    $mail->addBCC($em);
  }
  $mail->addReplyTo('swim@pacerswimwear.co.za', 'Pacer Swimwear');

  //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
  //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
  $mail->isHTML(true);                                  // Set email format to HTML

  $mail->Subject = $variables['subject'];
  $mail->Body    = $variables['body'];

  if(!$mail->send()) {
      // message('Message could not be sent.', 'info');
      // message('Mailer Error: ' . $mail->ErrorInfo, 'info');
  } else {
      // message('Message has been sent', 'info');
  }
}